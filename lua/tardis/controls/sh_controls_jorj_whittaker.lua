TARDIS:AddControl({
	id = "jorj_whittaker_colors",
	tip_text = "Colour Control",
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local skins = self.metadata.Interior.jorj_whittaker_skin_no
		local skin =  self:GetData("jorj_whittaker_skin", 0)
		skin = (skin + 1) % skins

		local skin_name = "skin" .. skin
		self:ApplyTextureSet(skin_name)
		self:ApplyLightState(skin_name)
		self:SetData("jorj_whittaker_skin", skin)
	end,
})

TARDIS:AddControl({
	id = "jorj_whittaker_console_light",
	tip_text = "Blue Stabilisers",
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local prefix = "models/jorj/whittaker/"
		local on = not self:GetData("jorj_whittaker_console_lights", false)
		self:SetData("jorj_whittaker_console_lights", on)
		local texture = on and self:GetPower() and "whittaker_consoleblue_blue" or "whittaker_consoleblue"
		self:ChangeTexture("jorj_whittaker_console", 1, texture, prefix)
	end,
})

TARDIS:AddControl({
	id = "jorj_whittaker_custard_cream",
	tip_text = "Color Theme",
	serveronly = true,
	power_independent = true,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self, ply, part)
		local cc_dispenser = self:GetPart("jorj_whittaker_cc_dispenser")
		local cc_cookie = self:GetPart("jorj_whittaker_cc_cookie")
		local hitbox = self:GetPart("jorj_whittaker_cc_cookie_hitbox")
		local pedal = self:GetPart("jorj_whittaker_pedal")

		if not IsValid(cc_dispenser)
			or not IsValid(cc_cookie)
			or not IsValid(hitbox)
			or not IsValid(pedal)
		then
			return
		end

		local on = not self:GetData("jorj_whittaker_cc", false)
		self:SetData("jorj_whittaker_cc", on)

		if not on and IsValid(ply) and ply:IsPlayer() then
			ply:SetHealth(ply:Health() + 2)
		end

		cc_cookie:SetVisible(on)
		cc_dispenser:SetOn(on)
		cc_cookie:SetOn(on)
		hitbox:SetCollide(on, true)
		pedal:SetCollide(not on, true)
	end,
})

TARDIS:AddControl({
	id = "jorj_whittaker_table",
	tip_text = "Table",
	serveronly = true,
	power_independent = false,
	screen_button = { virt_console = false, mmenu = false, },

	int_func=function(self,ply)
		local htable = self:GetPart("jorj_whittaker_hexraise")
		if not IsValid(htable) then return end

		htable:Toggle()
	end,
})

