local T = {
	Base = "jorj_whittaker_s13",
	Name = "Type 75 Master",
	ID = "jorj_whittaker_master",
	IsVersionOf="jorj_whittaker_s13",
	--IsVersionOf = "jorj_whittaker",

}
T.Interior={
	Parts={
		door={
				model="models/jorj/whittaker/int_doors_master.mdl",
		},
		jorj_whittaker_int_box=false,
		jorj_whittaker_int_box_master=true,
		jorj_whittaker_rotor=false,
		jorj_whittaker_rotor_master=true,
	},
	IdleSound={
		{
			path="jorj/whittaker/masterhum.wav",
			volume=1
		}
	},
	Lights={
		{ color = Color(245,66,66),	pos = Vector(-506,1,24),	brightness = 0.5,	nopower = false, shadows = true, },
	},
	Light = { color = Color(239,66,245),	pos = Vector(0,0,150),	brightness = 2,	nopower = false, },
	Lamps = {
		lamp1 = {
			color = Color(153, 35, 35),
			brightness=0.2,
		},
		lamp2 = {
			color = Color(82, 35, 168),
			brightness = 0.5,
		},
		lamp3 = {
			color = Color(153, 35, 108),
			brightness = 0.5,
		},
	},
	LightOverride = {
		basebrightness = 0,
		nopowerbrightness = 0
	},
}
T.Exterior={
	Model="models/jorj/whittaker/extbox_master.mdl",
	Parts={
		door={
			model="models/jorj/whittaker/ext_doors_master.mdl",posoffset=Vector(-4,0.2,35.5),angoffset=Angle(0,0,0),
		},
	},
}
T.CustomHooks = {
    initial_master_textures = {
        inthooks = { ["PostInitialize"] = true, },
        func = function(ext, int)
            int:ApplyTextureSet("master_interior")
        end,
    },
}
T.Interior.TextureSets = {
	["normal"] = {
		prefix = "models/jorj/whittaker/",
		base = "base",
	},
	["door_normal"] = {
		prefix = "models/jorj/whittaker/",
		base = "base_door_master",
	},
	["door_flight"] = {
		prefix = "models/jorj/whittaker/flight/",
		base = "base_door_master",
	},
	["door_off"] = {
		prefix = "models/jorj/whittaker/off/",
		base = "base_door_master",
	},
	["door_warning"] = {
		prefix = "models/jorj/whittaker/warning/",
		base = "base_door_master",
	},
	["door_warning_flight"] = {
		prefix = "models/jorj/whittaker/warning/",
		base = "base_door_master",
	},
	["master_interior"] = {
		{ "jorj_whittaker_pillar",               12,  "models/jorj/whittaker/whittaker_pillarbase_red"},
		{ "jorj_whittaker_pillar2",              12,  "models/jorj/whittaker/whittaker_pillarbase_red"},
		{ "jorj_whittaker_pillar3",              12,  "models/jorj/whittaker/whittaker_pillarbase_red"},
		{ "jorj_whittaker_pillar4",              12,  "models/jorj/whittaker/whittaker_pillarbase_red"},
		{ "jorj_whittaker_pillar5",              12,  "models/jorj/whittaker/whittaker_pillarbase_red"},
		{ "jorj_whittaker_pillar6",              12,  "models/jorj/whittaker/whittaker_pillarbase_red"},
		{ "jorj_whittaker_pillar",               11,  "models/jorj/whittaker/whittaker_pillar_master"},
		{ "jorj_whittaker_pillar2",              11,  "models/jorj/whittaker/whittaker_pillar_master"},
		{ "jorj_whittaker_pillar3",              11,  "models/jorj/whittaker/whittaker_pillar_master"},
		{ "jorj_whittaker_pillar4",              11,  "models/jorj/whittaker/whittaker_pillar_master"},
		{ "jorj_whittaker_pillar5",              11,  "models/jorj/whittaker/whittaker_pillar_master"},
		{ "jorj_whittaker_pillar6",              11,  "models/jorj/whittaker/whittaker_pillar_master"},
		{ "jorj_whittaker_console",              9,  "models/jorj/whittaker/whittaker_consolelight_master"},
		{ "jorj_whittaker_mainplatform",         9,  "models/jorj/whittaker/whittaker_crust_master"},
		{ "jorj_whittaker_platform2",            10,  "models/jorj/whittaker/whittaker_crust_master"},

		{ "jorj_whittaker_pillar",               5, },
		{ "jorj_whittaker_pillar2",              5, },
		{ "jorj_whittaker_pillar3",              5, },
		{ "jorj_whittaker_pillar4",              5, },
		{ "jorj_whittaker_pillar5",              5, },
		{ "jorj_whittaker_pillar6",              5, },

		{ "jorj_whittaker_console",              5, },
		{ "jorj_whittaker_bluehex1",             1, },
		{ "jorj_whittaker_hex2",                 1, },
		{ "jorj_whittaker_mainplatform",         2, },
		{ "jorj_whittaker_platform1",            5, },
		{ "jorj_whittaker_platform2",            2, },
		{ "jorj_whittaker_roofrotor",            1, },
		{ "jorj_whittaker_rotor",                2, },
		{ "jorj_whittaker_screens",              1, },
		{ "jorj_whittaker_stairs",               1, },
		{ "jorj_whittaker_underconsole",         5, },
		{ "jorj_whittaker_hexraise",             2, },
		{ "jorj_whittaker_roofitem",             1, },
		{ "jorj_whittaker_roofitem2",            1, },
		{ "jorj_whittaker_hexcrust",             5, },
		{ "jorj_whittaker_hexcrust2",            5, },
		{ "jorj_whittaker_stairshall",           2, },
		{ "jorj_whittaker_projector",            2, },
		{ "jorj_whittaker_hexcrust3",            2, },
		{ "jorj_whittaker_pillarextra_s13",      1, },
		{ "jorj_whittaker_pillarextra_s13_1",    1, },
		{ "jorj_whittaker_pillarextra_s13_2",    1, },
		{ "jorj_whittaker_sphere_s13",           2, },
		{ "jorj_whittaker_fliplamp_s13",         0, },
		{ "jorj_whittaker_fliplamp_s13_1",       0, },
		{ "jorj_whittaker_fliplamp_s13_2",       0, },
	},
}
TARDIS:AddInterior(T)