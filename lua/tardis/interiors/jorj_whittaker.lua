local T={}
T.Base="base"
T.Name ="2018-2022 TARDIS"
T.ID="jorj_whittaker"
T.Versions = {
	randomize = true,
	allow_custom = true,
	randomize_custom = false,

	main = {
		id = "jorj_whittaker",
	},
	other = {
		{
			name = "Series 11",
			id = "jorj_whittaker_s11",
		},
		{
			name = "Series 12",
			id = "jorj_whittaker_s12",
			},
		{
			name = "Series 13",
			id = "jorj_whittaker_s13",
		},
		{
			name = "Series 13 Fluxified (BETA)",
			id = "jorj_whittaker_flux",
		},
		{
			name = "HAHAHAHA AH AHAHA",
			id = "jorj_whittaker_master",
		},
	}
}

T.is_jorj_whittaker = true



T.Interior={
	Model="models/jorj/whittaker/interior.mdl",
	Portal={
		pos=Vector(-616,3.2,32),
		ang=Angle(0,0,0),
		width=80,
		height=100,
		thickness = -5,
	},
	Fallback={
		pos=Vector(-593,3.86,-10),
		ang=Angle(0,0,0)
	},
	ExitDistance=1000,
	IdleSound={
		{
			path="jorj/whittaker/hum.wav",
			volume=1
		}
	},
	ScreensEnabled = false,
	ScreenDistance = 500,
	Screens = {
		{
			pos = Vector(-41,-34,82),
			ang = Angle(0, 40, 90),
			width = 240,
			height = 175,
			visgui_rows = 4,
			power_off_black = false,
		}
	},
	Tips = {
		style = "coral",
		view_range_max = 70,
		view_range_min = 50,
	},
	Sounds={
		Teleport={
			demat="jorj/whittaker/ext_demat.wav",
			demat_fail = "jorj/whittaker/failed_demat.wav",
		},
		Door={
			enabled=true,
			open="jorj/whittaker/dooropen.wav",
			close="jorj/whittaker/doorclose.wav"
		},
		Power = {
			On = "jorj/whittaker/powerup.wav", -- Power On
			Off = "jorj/whittaker/powerdown.wav" -- Power Off
		},
		FlightLoop="jorj/whittaker/int_flight.wav"
	},
	LightOverride = {
		basebrightness = 0,
		nopowerbrightness = 0.03,
	},
	Light = {
		color = Color(224,157,32),
		pos = Vector(0,0,150),
		brightness = 2,
		nopower = false,
		warn_color = Color(255, 10, 0),

	},
	Lights={
		{
			color = Color(255,123,0),
			pos = Vector(-506,1,24),
			brightness = 0.5,
			nopower = false,
			shadows = true,
			warn_color = Color(255, 10, 0),
		},
		{
			color = Color(245,173,107),
			pos = Vector(-583,4,70),
			brightness = 0.15,
			nopower = false,
			shadows = true,
			warn_color = Color(255, 10, 0),
		},
		
	},
	Lamps = {
		lamp1 = {
			color = Color(47, 37, 197),
			texture = "effects/flashlight/gradient",
			fov = 600,
			distance = 2000,
			brightness = 3,
			pos =   Vector(-247.467, 343.407, 243.558),
			ang = Angle(10,-68,-23),
			nopower = true,
			shadows = true,
			sprite = true,
			off = {
				brightness=0.5,
				sprite = true,
				sprite_brightness = 2,
				color = Color(20, 35, 153),
			},
			warn = {
				-- same list of options available
				sprite_brightness = 0.5,
				color = Color(253, 4, 4),
			},

		},
		lamp2 = {
			color = Color(24, 62, 134),
			texture = "effects/flashlight/bars",
			fov = 600,
			distance = 2000,
			brightness = 3,
			pos = Vector(329,315,203),
			ang = Angle(28,-129,-23),
			nopower = true,
			shadows = true,
			sprite = true,
			sprite_brightness = 10,
			off = {
				brightness=0.5,
				sprite = true,
				sprite_brightness = 1,
				color = Color(20, 35, 153),
			},
			warn = {
				-- same list of options available
				sprite_brightness = 0.5,
				color = Color(253, 4, 4),
			},

		},
		lamp3 = {
			color = Color(21, 58, 114),
			texture = "effects/flashlight/gradient",
			fov = 300,
			distance = 2000,
			brightness = 3,
			pos = Vector(-276,-469,270),
			ang = Angle(59,99,49),
			nopower = true,
			shadows = true,
			sprite = true,
			off = {
				brightness=1,
				sprite = true,
				sprite_brightness = 0.1,
				color = Color(20, 35, 153),
			},
			warn = {
				-- same list of options available
				sprite_brightness = 0.5,
				color = Color(253, 4, 4),
			},

		},
	},

	Parts={
		door={
			model="models/jorj/whittaker/int_doors.mdl",
			posoffset=Vector(5,-21,-5.2),
			angoffset=Angle(0,0,0)
		},
		jorj_whittaker_wall1={	ang=Angle(0,0,0),pos=Vector(0,0,0) },
		jorj_whittaker_wall2={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_wall3={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_wall4={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_wall5={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_wall6={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_wall7={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_wall8={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_mainplatform={	ang=Angle(0,0,0),pos=Vector(-11,0,0)},
		jorj_whittaker_console={	ang=Angle(0,0,0),pos=Vector(0,0,-0)},
		jorj_whittaker_pipes={	ang=Angle(0,0,0),pos=Vector(0,0,1.7)},
		jorj_whittaker_bluehex1={	ang=Angle(0,0,0),pos=Vector(0,0,1)},
		jorj_whittaker_hexcrust={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_hex2={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_hexcrust2={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_stairs={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_stairshall={	ang=Angle(0,0,0),pos=Vector(0,0,-3)},
		jorj_whittaker_roofitem={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_roofitem2={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_roofrotor={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_pillar={	ang=Angle(0,0,0),pos=Vector(-3,-3,0)},
		jorj_whittaker_pillar2={	ang=Angle(0,-61,0),pos=Vector(-18,-4,0)},
		jorj_whittaker_pillar3={	ang=Angle(0,-122,0),pos=Vector(5,5,0)},
		jorj_whittaker_pillar4={	ang=Angle(0,-178,0),pos=Vector(6,12,0)},
		jorj_whittaker_pillar5={	ang=Angle(0,120,0),pos=Vector(25,0,0)},
		jorj_whittaker_pillar6={	ang=Angle(0,60,0),pos=Vector(25,5,0)},
		jorj_whittaker_mainlever={	ang=Angle(0,0,0),pos=Vector(5,30,70)},
		jorj_whittaker_mainleversecondary={ang=Angle(0,180,0),pos=Vector(5,-35,69)},
		jorj_whittaker_underconsole={	ang=Angle(0,0,0),pos=Vector(0,0,1)},
		jorj_whittaker_panel1stuff={	ang=Angle(0,0,0),pos=Vector(0,0,1.5)},
		jorj_whittaker_switch1={	ang=Angle(0,0,0),pos=Vector(-39.734,-1.5,54)},
		jorj_whittaker_switch1_1={	ang=Angle(0,18,90),pos=Vector(-36.7,0,55.5)},
		jorj_whittaker_switch2={	ang=Angle(0,0,0),pos=Vector(-33.7,-1.5,56.6)},
		jorj_whittaker_fliplamp={	ang=Angle(0,0,0),pos=Vector(-29.0,3.3,66)},
		jorj_whittaker_switch3={	ang=Angle(0,0,0),pos=Vector(-32.4,16.8,52.7)},
		jorj_whittaker_switch3_1={	ang=Angle(0,0,0),pos=Vector(-34.1,17.7,52.4)},
		jorj_whittaker_switch3_2={	ang=Angle(0,0,0),pos=Vector(-35.8,18.6,52.4)},
		jorj_whittaker_switch3_3={	ang=Angle(0,0,0),pos=Vector(-37.5,19.5,52.1)},
		jorj_whittaker_panel2stuff={	ang=Angle(0,0,0),pos=Vector(0,0,1)},
		jorj_whittaker_switch4={	ang=Angle(0,0,0),pos=Vector(-21.9,48.2,50.8)},
		jorj_whittaker_switch4_1={	ang=Angle(0,0,0),pos=Vector(-22.5,46,50.9)},
		jorj_whittaker_switch4_2={	ang=Angle(0,0,0),pos=Vector(-18.7,44,52.8)},
		jorj_whittaker_switch5={	ang=Angle(0,0,0),pos=Vector(-15.4,31.6,57.6)},
		jorj_whittaker_switch5_1={	ang=Angle(0,0,0),pos=Vector(-10.4,34.5,57.6)},
		jorj_whittaker_switch3_4={	ang=Angle(-0.6,-40,-08),pos=Vector(2.2,45.5,51.5)},
		jorj_whittaker_switch3_5={	ang=Angle(-0.6,-40,-08),pos=Vector(2.2,47.5,51.5)},
		jorj_whittaker_wheel={	ang=Angle(0,0,0),pos=Vector(3.2,52.5,50.5)},
		jorj_whittaker_panel3stuff={	ang=Angle(0,0,0),pos=Vector(0,0,1.5)},
		jorj_whittaker_switch7={	ang=Angle(0,0,0),pos=Vector(30.4,34.4,55.5)},
		jorj_whittaker_switch8={	ang=Angle(0,0,0),pos=Vector(28.6,35.4,55.5)},
		jorj_whittaker_wheel_1={	ang=Angle(0,-55,0),pos=Vector(52,28,50)},
		jorj_whittaker_wheel_2={	ang=Angle(0,-50,0),pos=Vector(34,18,60)},
		jorj_whittaker_switch3_6={	ang=Angle(-0.6,-40,-08),pos=Vector(8.4,48,51.85)},
		jorj_whittaker_switch3_7={	ang=Angle(-0.6,-40,-08),pos=Vector(8.4,46.3,52)},
		jorj_whittaker_switch3_8={	ang=Angle(-0.6,-40,-08),pos=Vector(8.3,44.5,52.2)},
		jorj_whittaker_switch3_9={	ang=Angle(-0.6,-40,-08),pos=Vector(8.2,42.3,52.4)},
		jorj_whittaker_switch3_10={	ang=Angle(30,90,10),pos=Vector(30.7,43.7,52)},
		jorj_whittaker_switch3_11={	ang=Angle(30,90,10),pos=Vector(28.8,40,53.7)},
		jorj_whittaker_switch3_12={	ang=Angle(30,90,10),pos=Vector(33.7,41.8,52.3)},
		jorj_whittaker_switch3_13={	ang=Angle(30,90,10),pos=Vector(31.8,38.5,53.7)},
		jorj_whittaker_fliplamp_2={	ang=Angle(0,-115,0),pos=Vector(27,24.5,67)},
		jorj_whittaker_hourglass={	ang=Angle(0,0,0),pos=Vector(23,20,80)},
		jorj_whittaker_panel4stuff={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_switch3_14={	ang=Angle(0,230,0),pos=Vector(45.3,17.7,52.2)},
		jorj_whittaker_switch3_15={	ang=Angle(0,230,0),pos=Vector(46.9,18.7,51.6)},
		jorj_whittaker_switch3_16={	ang=Angle(0,230,0),pos=Vector(48.4,19.7,51.3)},
		jorj_whittaker_switch3_17={	ang=Angle(0,245,0),pos=Vector(49.9,20.5,51)},
		jorj_whittaker_switch8_1={	ang=Angle(0,0,20),pos=Vector(57.7,-2,50)},
		jorj_whittaker_switch3_18={	ang=Angle(30,300,-30),pos=Vector(52.5,-2.2,52.9)},
		jorj_whittaker_switch5_2={	ang=Angle(0,105,130),pos=Vector(49,-2.8,55.3)},
		jorj_whittaker_panel5stuff={	ang=Angle(0,0,0),pos=Vector(0,0,3)},
		jorj_whittaker_spintardis={	ang=Angle(0,0,0),pos=Vector(29,-23,68)},
		jorj_whittaker_switch3_19={	ang=Angle(0,120,0),pos=Vector(8.2,-45.7,52.6)},
		jorj_whittaker_switch3_20={	ang=Angle(0,120,0),pos=Vector(8.15,-44.1,52.9)},
		jorj_whittaker_switch9={	ang=Angle(0,0,0),pos=Vector(8,-34, 61)},
		jorj_whittaker_hourglass_2={	ang=Angle(0,0,0),pos=Vector(-11,-22,80)},
		jorj_whittaker_panel6stuff={	ang=Angle(0,0,0),pos=Vector(0,0,1)},
		jorj_whittaker_switch8_2={	ang=Angle(0,-40,40),pos=Vector(-20,-44,52.5)},
		jorj_whittaker_switch5_3={	ang=Angle(0,40,20),pos=Vector(-14,-35,57)},
		jorj_whittaker_wheel_3={	ang=Angle(0,120,0),pos=Vector(-42,-31,50)},
		jorj_whittaker_porch={	ang=Angle(0,0,0),pos=Vector(10,1.5,1)},
		jorj_whittaker_int_box={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_hexoutline={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_blackwall={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_screens={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_rotor={	ang=Angle(0,45,0),pos=Vector(0,0,145)},
		jorj_whittaker_platform1={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_platform2={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_hexraise={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_projector={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_switch10={	ang=Angle(0,0,0),pos=Vector(4.9,-54.3,52)},
		jorj_whittaker_stairsdoor={	ang=Angle(0,0,0),pos=Vector(0,0,-2)},
		jorj_whittaker_pedal={	ang=Angle(0,0,0),pos=Vector(-31,-21,20)},
		jorj_whittaker_cc_dispenser={	ang=Angle(0,0,0), pos=Vector(-27,-16,45)},
		jorj_whittaker_cc_cookie={	ang=Angle(0,0,0), pos=Vector(-36.5,-21.5,40.5)},
		jorj_whittaker_cc_cookie_hitbox = { pos = Vector(-40.545, -21.572, 33.742), ang = Angle(-45.648, 22.95, 0.821), },
		jorj_whittaker_roofhex=true,
	},
	Controls = {
		jorj_whittaker_mainleversecondary = "teleport_double",
		jorj_whittaker_mainlever = "handbrake",
		jorj_whittaker_switch3_17 = "toggle_scanners",
		jorj_whittaker_switch7 = "door",
		jorj_whittaker_projector = "thirdperson",
		jorj_whittaker_panel1stuff = "destination",
		jorj_whittaker_switch5_2 = "power",
		jorj_whittaker_wheel_2 = "repair",
		jorj_whittaker_switch3_19 = "flight",
		jorj_whittaker_switch3_14 = "float",
		jorj_whittaker_switch9 = "vortex_flight",
		jorj_whittaker_switch3_4 = "physlock",
		jorj_whittaker_wheel_3 = "engine_release",
		jorj_whittaker_panel5stuff = "coordinates",
		jorj_whittaker_switch3_10 = "isomorphic",
		jorj_whittaker_switch3_3 = "jorj_whittaker_console_light",
		jorj_whittaker_switch8_1 = "jorj_whittaker_colors",
		jorj_whittaker_spintardis = "thirdperson",
		jorj_whittaker_switch5_3 = "toggle_screens",
		jorj_whittaker_switch5_1 = "hads",
		jorj_whittaker_wheel = "redecorate",
		jorj_whittaker_pedal = "jorj_whittaker_custard_cream",
		jorj_whittaker_cc_cookie_hitbox = "jorj_whittaker_custard_cream",
		jorj_whittaker_switch3_6 = "jorj_whittaker_table",
		jorj_whittaker_wheel_1 = "fastreturn",

	},
	PartTips = {
		jorj_whittaker_mainleversecondary = { pos = Vector(5.5,-33,72), },
		jorj_whittaker_switch7 = { pos = Vector(30.718, 34.202, 55.359), },
		jorj_whittaker_projector = { pos = Vector(5,44,69), },
		jorj_whittaker_panel1stuff = { pos = Vector(-27,17,71), },
		jorj_whittaker_mainlever = { pos = Vector(4,27,73), },
		jorj_whittaker_switch5_2 = { pos = Vector(48,-2,55.5), },
		jorj_whittaker_wheel_2 = { pos = Vector(32,17,60), },
		jorj_whittaker_switch3_19 = { pos = Vector(8,-45,53), },
		jorj_whittaker_switch3_14 = { pos = Vector(44,17,52), },
		jorj_whittaker_switch9 = { pos = Vector(8,-33,60), },
		jorj_whittaker_switch3_4 = { pos = Vector(2,45,51), },
		jorj_whittaker_wheel_3 = { pos = Vector(-41,-32,49), },
		jorj_whittaker_panel5stuff = { pos = Vector(29,-44,53), },
		jorj_whittaker_switch3_10 = { pos = Vector(30,43,51), },
		jorj_whittaker_switch3_17 = { pos = Vector(50,20,51), },
		jorj_whittaker_switch3_3 = { pos = Vector(-38,19,52), },
		jorj_whittaker_spintardis = { pos = Vector(30,-22,70), },
		jorj_whittaker_switch5_3 = { pos = Vector(-14,-35,57), },
		jorj_whittaker_switch8_1 = { pos = Vector(58,-1,52), },
		jorj_whittaker_switch5_1 = { pos = Vector(-10,35,58), },
		jorj_whittaker_wheel = { pos = Vector(2,52,49), },
		jorj_whittaker_wheel_1 = { pos = Vector(51,28,49), },
	},
	CustomTips = {
		{ pos = Vector(40,-27,68), control = "thirdperson", },
		{ pos = Vector(-35,-29,68), control = "thirdperson", },
		{ pos = Vector(8,47,52), text = "Hexagon Table", },
	},
	Scanners = {
		{
			mat = "models/jorj/whittaker/whittaker_grey",
			width = 1500,
			height = 1500,
			ang = Angle(0,0,0),
			fov = 100,
		}
	},
}

T.Exterior={
	Model="models/jorj/whittaker/extbox.mdl",
	Mass=5000,
	ScannerOffset=Vector(30,0,50),
	Portal={
		pos=Vector(29,0,54.08),
		ang=Angle(0,0,0),
		width = 49.7,
		height = 95,
		thickness = 22.021276595745,
		inverted = true,
	},
	Parts={
		door={
			model="models/jorj/whittaker/ext_doors.mdl",
			posoffset=Vector(-4,0.2,35.5),
			angoffset=Angle(0,0,0),
		},
		vortex = {
			model="models/jorj/whittaker/vortex.mdl",
			pos=Vector(0,0,50),
			ang=Angle(0,0,0),
			scale=10
		}
	},
	Fallback={
		pos=Vector(50,0,0),
		ang=Angle(0,0,0)
	},
	Light={
		color=Color(255,255,260),
		pos=Vector(0,0,114),
		brightness=3
	},
	Sounds={
		Teleport={
			demat="jorj/whittaker/ext_demat.wav",
			demat_fail = "jorj/whittaker/failed_demat_ext.wav",
		},
		Door={
			enabled=true,
			open="jorj/whittaker/dooropen.wav",
			close="jorj/whittaker/doorclose.wav"
		},
		FlightLoop="jorj/whittaker/ext_flight.wav"
	},

Teleport={
	{
	SequenceSpeed=0.2,
	SequenceSpeedFast=0.3,
	},
},
}





T.Interior.TextureSets = {
	["normal"] = {
		prefix = "models/jorj/whittaker/",
		base = "base",
	},
	["flight"] = {
		prefix = "models/jorj/whittaker/flight/",
		base = "base",
	},
	["off"] = {
		prefix = "models/jorj/whittaker/off/",
		base = "base",
	},
	["warning"] = {
		prefix = "models/jorj/whittaker/warning/",
		base = "base",
	},
	["warning_flight"] = {
		prefix = "models/jorj/whittaker/warning/",
		base = "base",
	},

	["door_normal"] = {
		prefix = "models/jorj/whittaker/",
		base = "base_door",
	},
	["door_flight"] = {
		prefix = "models/jorj/whittaker/flight/",
		base = "base_door",
	},
	["door_off"] = {
		prefix = "models/jorj/whittaker/off/",
		base = "base_door",
	},
	["door_warning"] = {
		prefix = "models/jorj/whittaker/warning/",
		base = "base_door",
	},
	["door_warning_flight"] = {
		prefix = "models/jorj/whittaker/warning/",
		base = "base_door",
	},

	["warning_rotor"] = {
		prefix = "models/jorj/whittaker/warning/",
		{"jorj_whittaker_rotor", 0, "whittaker_rotor"},
		{"jorj_whittaker_rotor", 1, "whittaker_rotor_red"},
		{"jorj_whittaker_rotor", 2, "whittaker_rotor_pink"},
		{"jorj_whittaker_rotor", 3, "whittaker_rotor_green"},
		{"jorj_whittaker_rotor", 4, "whittaker_rotor_white"},
		{"jorj_whittaker_rotor", 5, "whittaker_rotor_blue"},
	},
}
T.CustomHooks = {
	travel_textures = {
		exthooks = {
			["WhittakerTextureUpdate"] = true,
			["PowerToggled"] = true,
			["HealthWarningToggled"] = true,
			["DematStart"] = true,
			["MatStart"] = true,
			["PreMatStart"] = true,
			["StopDemat"] = true,
			["StopMat"] = true,
			["FlightToggled"] = true,
			["InterruptTeleport"] = true,
		},
		inthooks = {
			["PostInitialize"] = true,
		},
		func = function(ext, int, data_id, data_value)
			local flight = ext:GetData("flight")
			local teleport = ext:GetData("teleport")
			local vortex = ext:GetData("vortex")
			local power = ext:GetData("power-state")
			local warning = ext:GetData("health-warning", false)
			local active = flight or teleport or vortex

			local selected = "normal"

			if not power then
				selected = "off"
			elseif warning and active then
				selected = "warning_flight"
			elseif warning then
				selected = "warning"
			elseif active then
				selected = "flight"
			end

			int:ApplyTextureSet(selected)
			int:ApplyTextureSet("door_" .. selected)
		end
	},
	power_blue_lights = {
		exthooks = {
			["PowerToggled"] = true,
		},
		func = function(ext, int, power)
			local prefix = "models/jorj/whittaker/"
			local on = int:GetData("jorj_whittaker_console_lights", false) and power
			local texture = on and "whittaker_consoleblue_blue" or "whittaker_consoleblue"
			int:ChangeTexture("jorj_whittaker_console", 1, texture, prefix)
		end,
	},
	interior_door = {
		inthooks = {
			["ShouldDrawPart"] = true,
		},
		func = function(ext, int, part)
			if SERVER then return end
			if wp.drawing
				and wp.drawingent == int.portals.exterior
				and IsValid(part)
				and part == int:GetPart("door")
			then
				return false
			end
		end,
	},
	rotor_failed_demat = {
		exthooks = {
			["HandleNoDemat"] = true,
		},
		func = function(ext, int, part)
			int:ApplyTextureSet("warning_rotor")
			int:Timer("whittaker_rotor_warning", 2.5, function()
				ext:CallHook("WhittakerTextureUpdate")
			end)
		end,
	},
}
T.Templates = {
	["jorj_whittaker_textures"] = {},
}


T.Interior.jorj_whittaker_skin_no = 6

-- parts which need skins changing
local PartsWithSkins = {
	"jorj_whittaker_pillar",
	"jorj_whittaker_pillar2",
	"jorj_whittaker_pillar3",
	"jorj_whittaker_pillar4",
	"jorj_whittaker_pillar5",
	"jorj_whittaker_pillar6",
	"jorj_whittaker_console",
	"jorj_whittaker_bluehex1",
	"jorj_whittaker_hex2",
	"jorj_whittaker_mainplatform",
	"jorj_whittaker_platform1",
	"jorj_whittaker_platform2",
	"jorj_whittaker_platform2_s11",
	"jorj_whittaker_roofrotor",
	"jorj_whittaker_rotor",
	"jorj_whittaker_screens",
	"jorj_whittaker_stairs",
	"jorj_whittaker_underconsole",
	"jorj_whittaker_hexraise",
	"jorj_whittaker_roofitem",
	"jorj_whittaker_roofitem2",
	"jorj_whittaker_hexcrust",
	"jorj_whittaker_hexcrust2",
	"jorj_whittaker_stairshall",
	"jorj_whittaker_projector",
	"jorj_whittaker_hex3_s11",
	"jorj_whittaker_hexcrust3",
	"jorj_whittaker_mainplatform_s11",
	"jorj_whittaker_s11pillar",
	"jorj_whittaker_s11pillar_1",
	"jorj_whittaker_s11pillar_2",
	"jorj_whittaker_s11pillar_3",
	"jorj_whittaker_s11pillar_4",
	"jorj_whittaker_s11pillar_5",
	"jorj_whittaker_rotor_s11",
	"jorj_whittaker_pillarextra_s13",
	"jorj_whittaker_pillarextra_s13_1",
	"jorj_whittaker_pillarextra_s13_2",
	"jorj_whittaker_sphere_s13",
	"jorj_whittaker_fliplamp_s13",
	"jorj_whittaker_fliplamp_s13_1",
	"jorj_whittaker_fliplamp_s13_2",
	"jorj_whittaker_rotor_master",
	"jorj_whittaker_pillar_flux",
	"jorj_whittaker_pillar3_flux",
	"jorj_whittaker_screens_flux",
	"jorj_whittaker_bluehexextra_flux",
	"jorj_whittaker_bluehexextra_flux_2",
	"jorj_whittaker_fluxpart2_flux"
}

for i = 0, T.Interior.jorj_whittaker_skin_no - 1 do
	local ts = {}

	for k,v in ipairs(PartsWithSkins) do
		table.insert(ts, {v, i})
	end

	T.Interior.TextureSets["skin" .. i] = ts
end

TARDIS:AddInterior(T)

local skin_changing_parts = {
	["door"] = true,
	["jorj_whittaker_int_box"] = true,
	["jorj_whittaker_floorbox_flux"] = true,
	["jorj_whittaker_flux_intdoors_1"] = true,
	["jorj_whittaker_wall5box_flux"] = true,
	["jorj_whittaker_flux_intdoors_2"] = true,
	["jorj_whittaker_int_box_master"] = true,
}

hook.Add("SkinChanged", "tardis2_jorj_whittaker_skins", function(ent,i)
	if not ent.TardisExterior and not ent.TardisPart then return end

	local ext = ent.TardisPart and ent.exterior or ent
	local int = ent.interior

	if not ext.metadata.is_jorj_whittaker then return end
	if ent.TardisPart and not skin_changing_parts[ent.ID] then return end


	local function UpdateSkin(e)
		if IsValid(e) and e:GetSkin() ~= i then
			e:SetSkin(i)
		end
	end

	UpdateSkin(ext)
	UpdateSkin(ext:GetPart("door"))

	if IsValid(int) then
		for k,v in pairs(skin_changing_parts) do
			UpdateSkin(int:GetPart(k))
		end
	end
end)