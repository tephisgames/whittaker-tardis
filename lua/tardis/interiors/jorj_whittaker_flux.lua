local main_door = {
	pos = Vector(-616,3.2,32),
	ang = Angle(0,0,0),
	width = 80,
	height = 100,
	thickness = -5,
	inverted = false,

	door_model = "models/jorj/whittaker/int_doors.mdl",
	door_pos = Vector(-611.0, -17.799805, 26.799805 ),
	door_ang = Angle(0,0,0),

	fallback = Vector(-593,3.86,-10),
}

local floor_door = {
	ang = Angle(-95, 70, 230),
	pos = Vector(-309.1318, -116.5034, 33.969),
	width = 50,
	height = 100,


	thickness = 0,
	inverted = false,

	door_model = "models/jorj/whittaker/floorboxdoors_flux.mdl",
	door_pos = Vector(-287.734,-110.425,33.898),
	door_ang = Angle(95,260,140),

	fallback = Vector(-283.766, -161.943, 60.131),
}

local wall_door = {
	pos = Vector(489.05957, -48.03627, 59.2949),
	ang = Angle(0,180,6),
	width = 50,
	height = 100,
	thickness = -5,
	inverted = false,

	door_model = "models/jorj/whittaker/int_doors.mdl",
	door_pos = Vector(488,-27.5,52),
	door_ang = Angle(0,180,6),

	fallback = Vector(398.274, -49.369, 77.02),
}

local wall_door_in = table.Copy(wall_door)
local floor_door_in = table.Copy(floor_door)
local main_door_in = table.Copy(main_door)

wall_door_in.fallback = floor_door.fallback
wall_door_in.door_model = "models/jorj/whittaker/floorboxdoors_flux.mdl"

floor_door_in.fallback = wall_door.fallback

main_door_in.door_model = "models/jorj/whittaker/floorboxdoors_flux.mdl"
main_door_in.fallback = floor_door.fallback

local floor_door_in2 = table.Copy(floor_door_in)
floor_door_in2.pos = Vector(-309.24487, -116.8136, 37.744495)
floor_door_in2.fallback = main_door.fallback

local flux_portal_states = {
	{
		main_portal = main_door,
		fallback = {
			pos = main_door.fallback,
			ang = Angle(0,0,0)
		},
		custom_portal_door1 = floor_door,
		custom_portal_door2 = wall_door,
		exit_exterior = true,
	},
	{
		main_portal = wall_door,
		fallback={
			pos = wall_door.fallback,
			ang = Angle(0,0,0)
		},
		custom_portal_door1 = floor_door,
		custom_portal_door2 = main_door,
		exit_exterior = true,
	},
	{
		main_portal = floor_door,
		fallback = {
			pos = floor_door.fallback,
			ang = Angle(0,0,0)
		},
		custom_portal_door1 = main_door,
		custom_portal_door2 = wall_door,
		exit_exterior = true,
	},
	{
		main_portal = main_door,
		fallback = {
			pos = main_door.fallback,
			ang = Angle(0,0,0)
		},
		custom_portal_door1 = wall_door_in,
		custom_portal_door2 = floor_door_in,
		exit_exterior = false,
	},
	{
		main_portal = wall_door,
		fallback = {
			pos = wall_door.fallback,
			ang = Angle(0,0,0)
		},
		custom_portal_door1 = main_door_in,
		custom_portal_door2 = floor_door_in2,
		exit_exterior = false,
	},
}

local function MoveFluxPortal(int, portal, state, door)
	portal:SetPos(int:LocalToWorld(state.pos))
	portal:SetAngles(int:LocalToWorldAngles(state.ang))
	portal:SetWidth(state.width)
	portal:SetHeight(state.height)
	portal:SetThickness(state.thickness)
	portal:SetInverted(state.inverted)

	portal.fallback = state.fallback

	if IsValid(door) then
		door:SetModel(state.door_model)
		door:SetParent(NULL)
		door:SetPos(int:LocalToWorld(state.door_pos))
		door:SetAngles(int:LocalToWorldAngles(state.door_ang))
		door:SetParent(door.parent or NULL)
	end
end

local function ApplyFluxPortalState(int, k, ignore_order)
	if not IsValid(int) then return end

	if not ignore_order then
		int:SetData("flux_door_position", k, true)
	end

	local extp = int.portals.exterior
	local intp = int.portals.interior
	local custom1 = int.customportals["flux"].entry
	local custom2 = int.customportals["flux"].exit

	local s = flux_portal_states[k + 1]
	if not s then return end

	local m = s.main_portal
	local c1 = s.custom_portal_door1
	local c2 = s.custom_portal_door2

	if not m or not c1 or not c2 then return end

	local door = int:GetPart("door")
	local door1 = int:GetPart("jorj_whittaker_flux_intdoors_1")
	local door2 = int:GetPart("jorj_whittaker_flux_intdoors_2")

	int.Fallback = s.fallback

	MoveFluxPortal(int, intp, m, door)
	MoveFluxPortal(int, custom1, c1, door1)
	MoveFluxPortal(int, custom2, c2, door2)

	if s.exit_exterior then
		custom1:SetExit(extp)
		custom2:SetExit(extp)
	else
		custom1:SetExit(custom2)
		custom2:SetExit(custom1)
	end
	int:SetData("jwhittaker_sync_int_doors", not s.exit_exterior)
end

local T = {
	Base = "jorj_whittaker_s13",
	Name = "Series 13 Fluxified",
	ID = "jorj_whittaker_flux",
	IsVersionOf="jorj_whittaker_s13",
	--IsVersionOf = "jorj_whittaker",

}

T.Interior={
	Parts={
		jorj_whittaker_wall5 = false,
		jorj_whittaker_wall5_flux=true,

		jorj_whittaker_screens = false,
		jorj_whittaker_screens_flux = true,

		jorj_whittaker_floorbox_flux = { ang = Angle(95,260,140), pos = Vector(-290,-140,11) },
		jorj_whittaker_wall5box_flux = { ang = Angle(0,0,-6), pos = Vector(458,-54.5,29) },
		jorj_whittaker_hexraise_flux = true,
		jorj_whittaker_hexraise = false,
		jorj_whittaker_pillar = false,
		jorj_whittaker_pillar_flux = true,
		jorj_whittaker_pillar3_flux = { ang = Angle(0,-122,0), pos = Vector(5,5,0) },
		jorj_whittaker_pillar3 = false,
		jorj_whittaker_webs_flux = true,
		jorj_whittaker_bluehexextra_flux = true,
		jorj_whittaker_bluehexextra_flux_2 = { ang = Angle(0,-190,-0), pos = Vector(50,10,-19) },
		jorj_whittaker_fluxpart_flux = true,
		jorj_whittaker_fluxpart2_flux = true,
		jorj_whittaker_wall5_flux_collider = { ang = Angle(-4,-89,0), pos = Vector(408,-54,-14) },
		jorj_whittaker_wall5_flux_collider2 = { ang = Angle(0,0,0), pos = Vector(410,148,10) },

		jorj_whittaker_flux_intdoors_1 = {
			pos = floor_door.door_pos,
			ang = floor_door.door_ang,
		},
		jorj_whittaker_flux_intdoors_2 = {
			pos = wall_door.door_pos,
			ang = wall_door.door_ang,
		},
	},
	Lights={
		{
			color = Color(255,255,255),
			pos = Vector(446,-48,64),
			brightness = 0.1,
			nopower = false,
			shadows = true,
			warn_color = Color(255, 10, 0),
		},
	},
	CustomPortals = {
		flux = {
			entry = {
				pos = floor_door.pos,
				ang = floor_door.ang,
				width = floor_door.width,
				height = floor_door.height,
				thickness = floor_door.thickness,

				fallback = wall_door.fallback,
				link = "jorj_whittaker_flux_intdoors_1"
			},
			exit = {
				pos = wall_door.pos,
				ang = wall_door.ang,
				width = wall_door.width,
				height = wall_door.height,
				thickness = wall_door.thickness,

				fallback = floor_door.fallback,
				link = "jorj_whittaker_flux_intdoors_2"
			}
		},
	},
}

T.CustomHooks = {
	doors_sync = {
		exthooks = {
			["ToggleDoorReal"] = true,
		},
		func = function(ext, int, open)
			if open or CLIENT then return end
			local door1 = int:GetPart("jorj_whittaker_flux_intdoors_1")
			local door2 = int:GetPart("jorj_whittaker_flux_intdoors_2")

			if IsValid(door1) and door1:GetOn() then
				door1:Toggle(false)
			end
			if IsValid(door1) and door2:GetOn() then
				door2:Toggle(false)
			end
		end,
	},
	portal_random = {
		exthooks = {
			["ToggleDoor"] = true,
		},
		func = function(ext, int, open)
			if open or CLIENT then return end
			local door1 = int:GetPart("jorj_whittaker_flux_intdoors_1")
			local door2 = int:GetPart("jorj_whittaker_flux_intdoors_2")

			local close_time = int.metadata.Exterior.DoorAnimationTime

			if (IsValid(door1) and door1:GetOn()) or (IsValid(door2) and door2:GetOn()) then
				if door1:GetOn() then door1:Toggle(false) end
				if door2:GetOn() then door2:Toggle(false) end
				int:Timer("jwhittaker_doors_close", close_time, int.change_state)
			else
				int.change_state()
			end
		end,
	},
	portal_initial = {
		inthooks = {
			["PostInitialize"] = true,
		},
		func = function(ext, int)
			ApplyFluxPortalState(int, 0)
			int.ApplyFluxPortalState = ApplyFluxPortalState

			local door1 = int:GetPart("jorj_whittaker_flux_intdoors_1")
			local door2 = int:GetPart("jorj_whittaker_flux_intdoors_2")

			local states_total = #flux_portal_states

			int.change_state = function()
				if (IsValid(door1) and door1:GetOn()) or (IsValid(door2) and door2:GetOn()) then return end

				local p = int:GetData("flux_door_position", 0)

				p = (p + 1) % states_total
				ApplyFluxPortalState(int, p)
			end
		end,
	},
}

TARDIS:AddInterior(T)