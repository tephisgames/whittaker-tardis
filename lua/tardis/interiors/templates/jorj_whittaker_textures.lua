TARDIS:AddInteriorTemplate("jorj_whittaker_textures", {
	Interior = {
		TextureSets = {
			["base_door"] = {
				{"door", 0, "whittaker_intboxstuff"},
				{"door", 1, "whittaker_window"},
				{"door", 2, "whittaker_extwood_white"},
				{"door", 3, "whittaker_extwood"},
				{"door", 4, "whittaker_windowframe"},
				{"door", 5, "whittaker_extras"},
				{"door", 6, "whittaker_ptosign"},
				{"door", 7, "whittaker_window1_lit"},
				{"door", 8, "whittaker_window2"},
			},
			["base_door_master"] = {
				{"door", 0, "whittaker_intboxstuff"},
				{"door", 1, "whittaker_window"},
				{"door", 2, "whittaker_extwood_white"},
				{"door", 3, "whittaker_extwood_master"},
				{"door", 4, "whittaker_windowframe_master"},
				{"door", 5, "whittaker_extras_master"},
				{"door", 6, "whittaker_ptosign_master"},
				{"door", 7, "whittaker_window_master"},
				{"door", 8, "whittaker_window2"},
			},
			["base"] = {
				{"jorj_whittaker_console", 0, "whittaker_consolemain"},
				{"jorj_whittaker_console", 2, "whittaker_grey"},
				{"jorj_whittaker_console", 3, "whittaker_consolelight"},
				{"jorj_whittaker_console", 4, "whittaker_consolecorners"},
				{"jorj_whittaker_console", 5, "whittaker_consolelight_red"},
				{"jorj_whittaker_console", 6, "whittaker_consolelight_pink"},
				{"jorj_whittaker_console", 7, "whittaker_consolelight_green"},
				{"jorj_whittaker_console", 8, "whittaker_consolelight_white"},
				{"jorj_whittaker_console", 9, "whittaker_consolelight_blue"},

				{"jorj_whittaker_bluehex1", 0, "whittaker_hexbase"},
				{"jorj_whittaker_bluehex1", 1, "whittaker_bluehex"},
				{"jorj_whittaker_bluehex1", 10, "whittaker_bluehex_white"},
				{"jorj_whittaker_bluehex1", 11, "whittaker_hexglass_white"},
				{"jorj_whittaker_bluehex1", 2, "whittaker_hexglass"},
				{"jorj_whittaker_bluehex1", 3, "whittaker_hexreflect"},
				{"jorj_whittaker_bluehex1", 4, "whittaker_bluehex_red"},
				{"jorj_whittaker_bluehex1", 5, "whittaker_hexglass_red"},
				{"jorj_whittaker_bluehex1", 6, "whittaker_bluehex_pink"},
				{"jorj_whittaker_bluehex1", 7, "whittaker_hexglass_pink"},
				{"jorj_whittaker_bluehex1", 8, "whittaker_bluehex_green"},
				{"jorj_whittaker_bluehex1", 9, "whittaker_hexglass_green"},

				{"jorj_whittaker_hex2", 0, "whittaker_bluehex"},
				{"jorj_whittaker_hex2", 1, "whittaker_hexglass"},
				{"jorj_whittaker_hex2", 10, "whittaker_bluehex_white"},
				{"jorj_whittaker_hex2", 11, "whittaker_hexglass_white"},
				{"jorj_whittaker_hex2", 2, "whittaker_hexreflect"},
				{"jorj_whittaker_hex2", 3, "whittaker_hexbase"},
				{"jorj_whittaker_hex2", 4, "whittaker_bluehex_red"},
				{"jorj_whittaker_hex2", 5, "whittaker_hexglass_red"},
				{"jorj_whittaker_hex2", 6, "whittaker_bluehex_pink"},
				{"jorj_whittaker_hex2", 7, "whittaker_hexglass_pink"},
				{"jorj_whittaker_hex2", 8, "whittaker_bluehex_green"},
				{"jorj_whittaker_hex2", 9, "whittaker_hexglass_green"},

				{"jorj_whittaker_mainplatform", 0, "whittaker_platform3"},
				{"jorj_whittaker_mainplatform", 1, "whittaker_platform3light"},
				{"jorj_whittaker_mainplatform", 2, "whittaker_crust"},
				{"jorj_whittaker_mainplatform", 3, "whittaker_black"},
				{"jorj_whittaker_mainplatform", 4, "whittaker_platformlight"},
				{"jorj_whittaker_mainplatform", 5, "whittaker_platform3light_red"},
				{"jorj_whittaker_mainplatform", 6, "whittaker_crust_red"},
				{"jorj_whittaker_mainplatform", 7, "whittaker_platformlight_red"},
				{"jorj_whittaker_mainplatform", 8, "whittaker_platform3light_pink"},
				{"jorj_whittaker_mainplatform", 9, "whittaker_crust_pink"},
				{"jorj_whittaker_mainplatform", 10, "whittaker_platformlight_pink"},
				{"jorj_whittaker_mainplatform", 11, "whittaker_platform3light_green"},
				{"jorj_whittaker_mainplatform", 12, "whittaker_crust_green"},
				{"jorj_whittaker_mainplatform", 13, "whittaker_platformlight_green"},
				{"jorj_whittaker_mainplatform", 14, "whittaker_platform3light_white"},
				{"jorj_whittaker_mainplatform", 15, "whittaker_crust_white"},
				{"jorj_whittaker_mainplatform", 16, "whittaker_platformlight_white"},
				{"jorj_whittaker_mainplatform", 17, "whittaker_platform3light_blue"},
				{"jorj_whittaker_mainplatform", 18, "whittaker_crust_blue"},
				{"jorj_whittaker_mainplatform", 19, "whittaker_platformlight_blue"},
				

				{"jorj_whittaker_pillar", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar", 1, "whittaker_pillar"},
				{"jorj_whittaker_pillar", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_pillar", 12, "whittaker_pillarbase_blue"},
				{"jorj_whittaker_pillar", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_pillar", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_pillar", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_pillar", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar", 9, "whittaker_pillar_white"},

				{"jorj_whittaker_pillar2", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar2", 1, "whittaker_pillar"},
				{"jorj_whittaker_pillar2", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar2", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_pillar2", 12, "whittaker_pillarbase_blue"},
				{"jorj_whittaker_pillar2", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar2", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_pillar2", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar2", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_pillar2", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar2", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_pillar2", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar2", 9, "whittaker_pillar_white"},

				{"jorj_whittaker_pillar3", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar3", 1, "whittaker_pillar"},
				{"jorj_whittaker_pillar3", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar3", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_pillar3", 12, "whittaker_pillarbase_blue"},
				{"jorj_whittaker_pillar3", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar3", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_pillar3", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar3", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_pillar3", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar3", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_pillar3", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar3", 9, "whittaker_pillar_white"},

				{"jorj_whittaker_pillar4", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar4", 1, "whittaker_pillar"},
				{"jorj_whittaker_pillar4", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar4", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_pillar4", 12, "whittaker_pillarbase_blue"},
				{"jorj_whittaker_pillar4", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar4", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_pillar4", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar4", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_pillar4", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar4", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_pillar4", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar4", 9, "whittaker_pillar_white"},
				{"jorj_whittaker_pillar5", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar5", 1, "whittaker_pillar"},

				{"jorj_whittaker_pillar5", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar5", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_pillar5", 12, "whittaker_pillarbase_blue"},
				{"jorj_whittaker_pillar5", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar5", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_pillar5", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar5", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_pillar5", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar5", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_pillar5", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar5", 9, "whittaker_pillar_white"},

				{"jorj_whittaker_pillar6", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar6", 1, "whittaker_pillar"},
				{"jorj_whittaker_pillar6", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar6", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_pillar6", 12, "whittaker_pillarbase_blue"},
				{"jorj_whittaker_pillar6", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar6", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_pillar6", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar6", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_pillar6", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar6", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_pillar6", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar6", 9, "whittaker_pillar_white"},

				{"jorj_whittaker_platform2", 0, "whittaker_grey"},
				{"jorj_whittaker_platform2", 1, "whittaker_platform1hex"},
				{"jorj_whittaker_platform2", 2, "whittaker_platform2"},
				{"jorj_whittaker_platform2", 3, "whittaker_platformlight"},
				{"jorj_whittaker_platform2", 4, "whittaker_crust"},
				{"jorj_whittaker_platform2", 5, "whittaker_black"},
				{"jorj_whittaker_platform2", 6, "whittaker_platform1hex_red"},
				{"jorj_whittaker_platform2", 7, "whittaker_crust_red"},
				{"jorj_whittaker_platform2", 8, "whittaker_platformlight_red"},
				{"jorj_whittaker_platform2", 9, "whittaker_platform1hex_pink"},
				{"jorj_whittaker_platform2", 10, "whittaker_crust_pink"},
				{"jorj_whittaker_platform2", 11, "whittaker_platformlight_pink"},
				{"jorj_whittaker_platform2", 12, "whittaker_platform1hex_green"},
				{"jorj_whittaker_platform2", 13, "whittaker_crust_green"},
				{"jorj_whittaker_platform2", 14, "whittaker_platformlight_green"},
				{"jorj_whittaker_platform2", 15, "whittaker_platform1hex_white"},
				{"jorj_whittaker_platform2", 16, "whittaker_crust_white"},
				{"jorj_whittaker_platform2", 17, "whittaker_platformlight_white"},
				{"jorj_whittaker_platform2", 18, "whittaker_platform1hex_blue"},
				{"jorj_whittaker_platform2", 19, "whittaker_crust_blue"},
				{"jorj_whittaker_platform2", 20, "whittaker_platformlight_blue"},
				

				{"jorj_whittaker_roofrotor", 0, "whittaker_rotor"},
				{"jorj_whittaker_roofrotor", 1, "whittaker_rotor_red"},
				{"jorj_whittaker_roofrotor", 2, "whittaker_rotor_pink"},
				{"jorj_whittaker_roofrotor", 3, "whittaker_rotor_green"},
				{"jorj_whittaker_roofrotor", 4, "whittaker_rotor_white"},
				{"jorj_whittaker_roofrotor", 5, "whittaker_rotor_blue"},

				{"jorj_whittaker_rotor", 0, "whittaker_rotor"},
				{"jorj_whittaker_rotor", 1, "whittaker_rotor_red"},
				{"jorj_whittaker_rotor", 2, "whittaker_rotor_pink"},
				{"jorj_whittaker_rotor", 3, "whittaker_rotor_green"},
				{"jorj_whittaker_rotor", 4, "whittaker_rotor_white"},
				{"jorj_whittaker_rotor", 5, "whittaker_rotor_blue"},

				{"jorj_whittaker_screens", 0, "whittaker_screen1"},
				{"jorj_whittaker_screens", 1, "whittaker_screen2"},
				{"jorj_whittaker_screens", 10, "whittaker_screen1_blue"},
				{"jorj_whittaker_screens", 11, "whittaker_screen2_blue"},
				{"jorj_whittaker_screens", 2, "whittaker_screen1_red"},
				{"jorj_whittaker_screens", 3, "whittaker_screen2_red"},
				{"jorj_whittaker_screens", 4, "whittaker_screen1_pink"},
				{"jorj_whittaker_screens", 5, "whittaker_screen2_pink"},
				{"jorj_whittaker_screens", 6, "whittaker_screen1_green"},
				{"jorj_whittaker_screens", 7, "whittaker_screen2_green"},
				{"jorj_whittaker_screens", 8, "whittaker_screen1_white"},
				{"jorj_whittaker_screens", 9, "whittaker_screen2_white"},

				{"jorj_whittaker_stairs", 0, "whittaker_hexbase"},
				{"jorj_whittaker_stairs", 1, "whittaker_bluehex"},
				{"jorj_whittaker_stairs", 10, "whittaker_stairs_pink"},
				{"jorj_whittaker_stairs", 11, "whittaker_bluehex_green"},
				{"jorj_whittaker_stairs", 12, "whittaker_hexglass_green"},
				{"jorj_whittaker_stairs", 13, "whittaker_stairs_green"},
				{"jorj_whittaker_stairs", 14, "whittaker_bluehex_white"},
				{"jorj_whittaker_stairs", 15, "whittaker_hexglass_white"},
				{"jorj_whittaker_stairs", 16, "whittaker_stairs_white"},
				{"jorj_whittaker_stairs", 2, "whittaker_hexreflect"},
				{"jorj_whittaker_stairs", 3, "whittaker_hexglass"},
				{"jorj_whittaker_stairs", 4, "whittaker_stairs"},
				{"jorj_whittaker_stairs", 5, "whittaker_bluehex_red"},
				{"jorj_whittaker_stairs", 6, "whittaker_hexglass_red"},
				{"jorj_whittaker_stairs", 7, "whittaker_stairs_red"},
				{"jorj_whittaker_stairs", 8, "whittaker_bluehex_pink"},
				{"jorj_whittaker_stairs", 9, "whittaker_hexglass_pink"},
				{"jorj_whittaker_stairs", 17, "whittaker_stairs_blue"},

				{"jorj_whittaker_underconsole", 0, "whittaker_consolelight"},
				{"jorj_whittaker_underconsole", 1, "whittaker_grey"},
				{"jorj_whittaker_underconsole", 2, "whittaker_consolelight_red"},
				{"jorj_whittaker_underconsole", 3, "whittaker_consolelight_pink"},
				{"jorj_whittaker_underconsole", 4, "whittaker_consolelight_green"},
				{"jorj_whittaker_underconsole", 5, "whittaker_consolelight_white"},
				{"jorj_whittaker_underconsole", 6, "whittaker_consolelight_blue"},

				{"jorj_whittaker_hexraise", 0, "whittaker_hexraise"},
				{"jorj_whittaker_hexraise", 1, "whittaker_grey"},
				{"jorj_whittaker_hexraise", 2, "whittaker_platform1hex"},
				{"jorj_whittaker_hexraise", 3, "whittaker_platform1hex_red"},
				{"jorj_whittaker_hexraise", 4, "whittaker_hexraise_red"},
				{"jorj_whittaker_hexraise", 5, "whittaker_platform1hex_pink"},
				{"jorj_whittaker_hexraise", 6, "whittaker_hexraise_pink"},
				{"jorj_whittaker_hexraise", 7, "whittaker_platform1hex_green"},
				{"jorj_whittaker_hexraise", 8, "whittaker_hexraise_green"},
				{"jorj_whittaker_hexraise", 9, "whittaker_platform1hex_white"},
				{"jorj_whittaker_hexraise", 10, "whittaker_hexraise_white"},
				{"jorj_whittaker_hexraise", 11, "whittaker_platform1hex_blue"},
				{"jorj_whittaker_hexraise", 12, "whittaker_hexraise_blue"},
				

				{"jorj_whittaker_roofitem", 0, "whittaker_bluehex"},
				{"jorj_whittaker_roofitem", 1, "whittaker_hexglass"},
				{"jorj_whittaker_roofitem", 2, "whittaker_hexreflect"},
				{"jorj_whittaker_roofitem", 3, "whittaker_bluehex_red"},
				{"jorj_whittaker_roofitem", 4, "whittaker_hexglass_red"},
				{"jorj_whittaker_roofitem", 5, "whittaker_bluehex_pink"},
				{"jorj_whittaker_roofitem", 6, "whittaker_hexglass_pink"},
				{"jorj_whittaker_roofitem", 7, "whittaker_bluehex_green"},
				{"jorj_whittaker_roofitem", 8, "whittaker_hexglass_green"},
				{"jorj_whittaker_roofitem", 9, "whittaker_bluehex_white"},
				{"jorj_whittaker_roofitem", 10, "whittaker_hexglass_white"},


				{"jorj_whittaker_roofitem2", 0, "whittaker_bluehex"},
				{"jorj_whittaker_roofitem2", 1, "whittaker_hexglass"},
				{"jorj_whittaker_roofitem2", 2, "whittaker_hexreflect"},
				{"jorj_whittaker_roofitem2", 3, "whittaker_bluehex_red"},
				{"jorj_whittaker_roofitem2", 4, "whittaker_hexglass_red"},
				{"jorj_whittaker_roofitem2", 5, "whittaker_bluehex_pink"},
				{"jorj_whittaker_roofitem2", 6, "whittaker_hexglass_pink"},
				{"jorj_whittaker_roofitem2", 7, "whittaker_bluehex_green"},
				{"jorj_whittaker_roofitem2", 8, "whittaker_hexglass_green"},
				{"jorj_whittaker_roofitem2", 9, "whittaker_bluehex_white"},
				{"jorj_whittaker_roofitem2", 10, "whittaker_hexglass_white"},

				{"jorj_whittaker_hexcrust", 0, "whittaker_hexcrust"},
				{"jorj_whittaker_hexcrust", 1, "whittaker_hexcrust_red"},
				{"jorj_whittaker_hexcrust", 2, "whittaker_hexcrust_pink"},
				{"jorj_whittaker_hexcrust", 3, "whittaker_hexcrust_green"},
				{"jorj_whittaker_hexcrust", 4, "whittaker_hexcrust_white"},
				{"jorj_whittaker_hexcrust", 5, "whittaker_hexcrust_blue"},

				{"jorj_whittaker_hexcrust2", 0, "whittaker_hexcrust"},
				{"jorj_whittaker_hexcrust2", 1, "whittaker_hexcrust_red"},
				{"jorj_whittaker_hexcrust2", 2, "whittaker_hexcrust_pink"},
				{"jorj_whittaker_hexcrust2", 3, "whittaker_hexcrust_green"},
				{"jorj_whittaker_hexcrust2", 4, "whittaker_hexcrust_white"},
				{"jorj_whittaker_hexcrust2", 5, "whittaker_hexcrust_blue"},

				{"jorj_whittaker_stairshall", 0, "whittaker_hexagon_spec"},
				{"jorj_whittaker_stairshall", 1, "whittaker_bluehall"},
				{"jorj_whittaker_stairshall", 2, "whittaker_bluehall_red"},
				{"jorj_whittaker_stairshall", 3, "whittaker_bluehall_pink"},
				{"jorj_whittaker_stairshall", 4, "whittaker_bluehall_green"},
				{"jorj_whittaker_stairshall", 5, "whittaker_bluehall_white"},

				{"jorj_whittaker_spintardis", 0, "whittaker_spintardis"},

				{"jorj_whittaker_mainplatform_s11", 0, "whittaker_platform3"},
				{"jorj_whittaker_mainplatform_s11", 1, "whittaker_platform3light"},
				{"jorj_whittaker_mainplatform_s11", 2, "whittaker_symbols"},
				{"jorj_whittaker_mainplatform_s11", 3, "whittaker_black"},
				{"jorj_whittaker_mainplatform_s11", 4, "whittaker_platformlight"},
				{"jorj_whittaker_mainplatform_s11", 5, "whittaker_platform3light_red"},
				{"jorj_whittaker_mainplatform_s11", 6, "whittaker_symbols_red"},
				{"jorj_whittaker_mainplatform_s11", 7, "whittaker_platformlight_red"},
				{"jorj_whittaker_mainplatform_s11", 8, "whittaker_platform3light_pink"},
				{"jorj_whittaker_mainplatform_s11", 9, "whittaker_symbols_pink"},
				{"jorj_whittaker_mainplatform_s11", 10, "whittaker_platformlight_pink"},
				{"jorj_whittaker_mainplatform_s11", 11, "whittaker_platform3light_green"},
				{"jorj_whittaker_mainplatform_s11", 12, "whittaker_symbols_green"},
				{"jorj_whittaker_mainplatform_s11", 13, "whittaker_platformlight_green"},
				{"jorj_whittaker_mainplatform_s11", 14, "whittaker_platform3light_white"},
				{"jorj_whittaker_mainplatform_s11", 15, "whittaker_symbols_white"},
				{"jorj_whittaker_mainplatform_s11", 16, "whittaker_platformlight_white"},
				{"jorj_whittaker_mainplatform_s11", 17, "whittaker_platform3light_blue"},
				{"jorj_whittaker_mainplatform_s11", 18, "whittaker_symbols_blue"},
				{"jorj_whittaker_mainplatform_s11", 19, "whittaker_platformlight_blue"},


				{"jorj_whittaker_platform2_s11", 0, "whittaker_grey"},
				{"jorj_whittaker_platform2_s11", 1, "whittaker_platform1hex"},
				{"jorj_whittaker_platform2_s11", 2, "whittaker_platform2"},
				{"jorj_whittaker_platform2_s11", 3, "whittaker_platformlight"},
				{"jorj_whittaker_platform2_s11", 4, "whittaker_symbols"},
				{"jorj_whittaker_platform2_s11", 5, "whittaker_platform1hex_red"},
				{"jorj_whittaker_platform2_s11", 6, "whittaker_symbols_red"},
				{"jorj_whittaker_platform2_s11", 7, "whittaker_platformlight_red"},
				{"jorj_whittaker_platform2_s11", 8, "whittaker_platform1hex_pink"},
				{"jorj_whittaker_platform2_s11", 9, "whittaker_symbols_pink"},
				{"jorj_whittaker_platform2_s11", 10, "whittaker_platformlight_pink"},
				{"jorj_whittaker_platform2_s11", 11, "whittaker_platform1hex_green"},
				{"jorj_whittaker_platform2_s11", 12, "whittaker_symbols_green"},
				{"jorj_whittaker_platform2_s11", 13, "whittaker_platformlight_green"},
				{"jorj_whittaker_platform2_s11", 14, "whittaker_platform1hex_white"},
				{"jorj_whittaker_platform2_s11", 15, "whittaker_symbols_white"},
				{"jorj_whittaker_platform2_s11", 16, "whittaker_platformlight_white"},
				{"jorj_whittaker_platform2_s11", 17, "whittaker_platform1hex_blue"},
				{"jorj_whittaker_platform2_s11", 18, "whittaker_symbols_blue"},
				{"jorj_whittaker_platform2_s11", 19, "whittaker_platformlight_blue"},


				{"jorj_whittaker_hex3_s11", 0, "whittaker_bluehex"},
				{"jorj_whittaker_hex3_s11", 1, "whittaker_hexglass"},
				{"jorj_whittaker_hex3_s11", 2, "whittaker_hexreflect"},
				{"jorj_whittaker_hex3_s11", 3, "whittaker_hexbase"},
				{"jorj_whittaker_hex3_s11", 4, "whittaker_bluehex_red"},
				{"jorj_whittaker_hex3_s11", 5, "whittaker_hexglass_red"},
				{"jorj_whittaker_hex3_s11", 6, "whittaker_bluehex_pink"},
				{"jorj_whittaker_hex3_s11", 7, "whittaker_hexglass_pink"},
				{"jorj_whittaker_hex3_s11", 8, "whittaker_bluehex_green"},
				{"jorj_whittaker_hex3_s11", 9, "whittaker_hexglass_green"},
				{"jorj_whittaker_hex3_s11", 10, "whittaker_bluehex_white"},
				{"jorj_whittaker_hex3_s11", 11, "whittaker_hexglass_white"},

				{"jorj_whittaker_hexcrust3", 0, "whittaker_hexcrust"},
				{"jorj_whittaker_hexcrust3", 1, "whittaker_hexcrust_red"},
				{"jorj_whittaker_hexcrust3", 2, "whittaker_hexcrust_pink"},
				{"jorj_whittaker_hexcrust3", 3, "whittaker_hexcrust_green"},
				{"jorj_whittaker_hexcrust3", 4, "whittaker_hexcrust_white"},
				{"jorj_whittaker_hexcrust3", 5, "whittaker_hexcrust_blue"},

				{"jorj_whittaker_s11pillar", 0, "whittaker_pillar_s11"},
				{"jorj_whittaker_s11pillar", 1, "whittaker_pillarbase_s11"},
				{"jorj_whittaker_s11pillar", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_s11pillar", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_s11pillar", 4, "whittaker_pillarbase_s11_red"},
				{"jorj_whittaker_s11pillar", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_s11pillar", 6, "whittaker_pillarbase_s11_pink"},
				{"jorj_whittaker_s11pillar", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_s11pillar", 8, "whittaker_pillarbase_s11_green"},
				{"jorj_whittaker_s11pillar", 9, "whittaker_pillar_white"},
				{"jorj_whittaker_s11pillar", 10, "whittaker_pillarbase_s11_white"},
				{"jorj_whittaker_s11pillar", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_s11pillar", 12, "whittaker_pillarbase_s11_blue"},

				{"jorj_whittaker_s11pillar_1", 0, "whittaker_pillar_s11"},
				{"jorj_whittaker_s11pillar_1", 1, "whittaker_pillarbase_s11"},
				{"jorj_whittaker_s11pillar_1", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_s11pillar_1", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_s11pillar_1", 4, "whittaker_pillarbase_s11_red"},
				{"jorj_whittaker_s11pillar_1", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_s11pillar_1", 6, "whittaker_pillarbase_s11_pink"},
				{"jorj_whittaker_s11pillar_1", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_s11pillar_1", 8, "whittaker_pillarbase_s11_green"},
				{"jorj_whittaker_s11pillar_1", 9, "whittaker_pillar_white"},
				{"jorj_whittaker_s11pillar_1", 10, "whittaker_pillarbase_s11_white"},
				{"jorj_whittaker_s11pillar_1", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_s11pillar_1", 12, "whittaker_pillarbase_s11_blue"},

				{"jorj_whittaker_s11pillar_5", 0, "whittaker_pillar_s11"},
				{"jorj_whittaker_s11pillar_5", 1, "whittaker_pillarbase_s11"},
				{"jorj_whittaker_s11pillar_5", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_s11pillar_5", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_s11pillar_5", 4, "whittaker_pillarbase_s11_red"},
				{"jorj_whittaker_s11pillar_5", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_s11pillar_5", 6, "whittaker_pillarbase_s11_pink"},
				{"jorj_whittaker_s11pillar_5", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_s11pillar_5", 8, "whittaker_pillarbase_s11_green"},
				{"jorj_whittaker_s11pillar_5", 9, "whittaker_pillar_white"},
				{"jorj_whittaker_s11pillar_5", 10, "whittaker_pillarbase_s11_white"},
				{"jorj_whittaker_s11pillar_5", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_s11pillar_5", 12, "whittaker_pillarbase_s11_blue"},

				{"jorj_whittaker_s11pillar_3", 0, "whittaker_pillar_s11"},
				{"jorj_whittaker_s11pillar_3", 1, "whittaker_pillarbase_s11"},
				{"jorj_whittaker_s11pillar_3", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_s11pillar_3", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_s11pillar_3", 4, "whittaker_pillarbase_s11_red"},
				{"jorj_whittaker_s11pillar_3", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_s11pillar_3", 6, "whittaker_pillarbase_s11_pink"},
				{"jorj_whittaker_s11pillar_3", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_s11pillar_3", 8, "whittaker_pillarbase_s11_green"},
				{"jorj_whittaker_s11pillar_3", 9, "whittaker_pillar_white"},
				{"jorj_whittaker_s11pillar_3", 10, "whittaker_pillarbase_s11_white"},
				{"jorj_whittaker_s11pillar_3", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_s11pillar_3", 12, "whittaker_pillarbase_s11_blue"},

				{"jorj_whittaker_s11pillar_4", 0, "whittaker_pillar_s11"},
				{"jorj_whittaker_s11pillar_4", 1, "whittaker_pillarbase_s11"},
				{"jorj_whittaker_s11pillar_4", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_s11pillar_4", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_s11pillar_4", 4, "whittaker_pillarbase_s11_red"},
				{"jorj_whittaker_s11pillar_4", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_s11pillar_4", 6, "whittaker_pillarbase_s11_pink"},
				{"jorj_whittaker_s11pillar_4", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_s11pillar_4", 8, "whittaker_pillarbase_s11_green"},
				{"jorj_whittaker_s11pillar_4", 9, "whittaker_pillar_white"},
				{"jorj_whittaker_s11pillar_4", 10, "whittaker_pillarbase_s11_white"},
				{"jorj_whittaker_s11pillar_4", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_s11pillar_4", 12, "whittaker_pillarbase_s11_blue"},

				{"jorj_whittaker_s11pillar_2", 0, "whittaker_pillar_s11"},
				{"jorj_whittaker_s11pillar_2", 1, "whittaker_pillarbase_s11"},
				{"jorj_whittaker_s11pillar_2", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_s11pillar_2", 3, "whittaker_pillar_red"},
				{"jorj_whittaker_s11pillar_2", 4, "whittaker_pillarbase_s11_red"},
				{"jorj_whittaker_s11pillar_2", 5, "whittaker_pillar_pink"},
				{"jorj_whittaker_s11pillar_2", 6, "whittaker_pillarbase_s11_pink"},
				{"jorj_whittaker_s11pillar_2", 7, "whittaker_pillar_green"},
				{"jorj_whittaker_s11pillar_2", 8, "whittaker_pillarbase_s11_green"},
				{"jorj_whittaker_s11pillar_2", 9, "whittaker_pillar_white"},
				{"jorj_whittaker_s11pillar_2", 10, "whittaker_pillarbase_s11_white"},
				{"jorj_whittaker_s11pillar_2", 11, "whittaker_pillar_blue"},
				{"jorj_whittaker_s11pillar_2", 12, "whittaker_pillarbase_s11_blue"},

				{"jorj_whittaker_rotor_s11", 0, "whittaker_rotor_s11"},
				{"jorj_whittaker_rotor_s11", 1, "whittaker_rotor_red"},
				{"jorj_whittaker_rotor_s11", 2, "whittaker_rotor_pink"},
				{"jorj_whittaker_rotor_s11", 3, "whittaker_rotor_green"},
				{"jorj_whittaker_rotor_s11", 4, "whittaker_rotor_white"},
				{"jorj_whittaker_rotor_s11", 5, "whittaker_rotor_blue"},

				{"jorj_whittaker_pillarextra_s13_2", 0, "whittaker_hexglass"},
				{"jorj_whittaker_pillarextra_s13_2", 1, "whittaker_pillarbase"},
				{"jorj_whittaker_pillarextra_s13_2", 2, "whittaker_consoleblue"},
				{"jorj_whittaker_pillarextra_s13_2", 3, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillarextra_s13_2", 4, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillarextra_s13_2", 5, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillarextra_s13_2", 6, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillarextra_s13_2", 7, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_pillarextra_s13", 0, "whittaker_hexglass"},
				{"jorj_whittaker_pillarextra_s13", 1, "whittaker_pillarbase"},
				{"jorj_whittaker_pillarextra_s13", 2, "whittaker_consoleblue"},
				{"jorj_whittaker_pillarextra_s13", 3, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillarextra_s13", 4, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillarextra_s13", 5, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillarextra_s13", 6, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillarextra_s13", 7, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_pillarextra_s13_1", 0, "whittaker_hexglass"},
				{"jorj_whittaker_pillarextra_s13_1", 1, "whittaker_pillarbase"},
				{"jorj_whittaker_pillarextra_s13_1", 2, "whittaker_consoleblue"},
				{"jorj_whittaker_pillarextra_s13_1", 3, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillarextra_s13_1", 4, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillarextra_s13_1", 5, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillarextra_s13_1", 6, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillarextra_s13_1", 7, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_sphere_s13", 0, "whittaker_pillarjoint"},
				{"jorj_whittaker_sphere_s13", 1, "whittaker_sphere_s13"},
				{"jorj_whittaker_sphere_s13", 2, "whittaker_sphere_s13_red"},
				{"jorj_whittaker_sphere_s13", 3, "whittaker_sphere_s13_pink"},
				{"jorj_whittaker_sphere_s13", 4, "whittaker_sphere_s13_green"},
				{"jorj_whittaker_sphere_s13", 5, "whittaker_sphere_s13_white"},
				{"jorj_whittaker_sphere_s13", 6, "whittaker_sphere_s13_blue"},

				{"jorj_whittaker_fliplamp_s13_1", 0, "whittaker_fliplamp_s13"},
				{"jorj_whittaker_fliplamp_s13_1", 1, "whittaker_fliplamp_s13_red"},
				{"jorj_whittaker_fliplamp_s13_1", 2, "whittaker_fliplamp_s13_pink"},
				{"jorj_whittaker_fliplamp_s13_1", 3, "whittaker_fliplamp_s13_green"},
				{"jorj_whittaker_fliplamp_s13_1", 4, "whittaker_fliplamp_s13_white"},
				{"jorj_whittaker_fliplamp_s13_1", 5, "whittaker_fliplamp_s13_blue"},

				{"jorj_whittaker_fliplamp_s13", 0, "whittaker_fliplamp_s13"},
				{"jorj_whittaker_fliplamp_s13", 1, "whittaker_fliplamp_s13_red"},
				{"jorj_whittaker_fliplamp_s13", 2, "whittaker_fliplamp_s13_pink"},
				{"jorj_whittaker_fliplamp_s13", 3, "whittaker_fliplamp_s13_green"},
				{"jorj_whittaker_fliplamp_s13", 4, "whittaker_fliplamp_s13_white"},
				{"jorj_whittaker_fliplamp_s13", 5, "whittaker_fliplamp_s13_blue"},

				{"jorj_whittaker_fliplamp_s13_2", 0, "whittaker_fliplamp_s13"},
				{"jorj_whittaker_fliplamp_s13_2", 1, "whittaker_fliplamp_s13_red"},
				{"jorj_whittaker_fliplamp_s13_2", 2, "whittaker_fliplamp_s13_pink"},
				{"jorj_whittaker_fliplamp_s13_2", 3, "whittaker_fliplamp_s13_green"},
				{"jorj_whittaker_fliplamp_s13_2", 4, "whittaker_fliplamp_s13_white"},
				{"jorj_whittaker_fliplamp_s13_2", 5, "whittaker_fliplamp_s13_blue"},

				{"jorj_whittaker_platform1", 0, "whittaker_platform1hex"},
				{"jorj_whittaker_platform1", 1, "whittaker_platform1"},
				{"jorj_whittaker_platform1", 2, "whittaker_floor"},
				{"jorj_whittaker_platform1", 3, "whittaker_grey"},
				{"jorj_whittaker_platform1", 4, "whittaker_platform1hex_red"},
				{"jorj_whittaker_platform1", 5, "whittaker_platform1hex_pink"},
				{"jorj_whittaker_platform1", 6, "whittaker_platform1hex_green"},
				{"jorj_whittaker_platform1", 7, "whittaker_platform1hex_white"},
				{"jorj_whittaker_platform1", 8, "whittaker_platform1hex_blue"},

				{"jorj_whittaker_int_box", 0, "whittaker_intboxstuff"},
				{"jorj_whittaker_int_box", 1, "whittaker_extwood"},
				{"jorj_whittaker_int_box", 2, "whittaker_extwood_white"},
				{"jorj_whittaker_int_box", 3, "whittaker_window"},
				{"jorj_whittaker_int_box", 4, "whittaker_windowframe"},
				{"jorj_whittaker_int_box", 5, "whittaker_lamp"},
				{"jorj_whittaker_int_box", 6, "whittaker_policeboxsign"},
				{"jorj_whittaker_int_box", 7, "whittaker_hexglass"},
				{"jorj_whittaker_int_box", 8, "whittaker_window1_lit"},
				{"jorj_whittaker_int_box", 9, "whittaker_policeboxsign1_lit"},
				{"jorj_whittaker_int_box", 10, "whittaker_window2"},
				{"jorj_whittaker_int_box", 11, "whittaker_policeboxsign2"},
				

				{"jorj_whittaker_rotor_master", 0, "whittaker_pillarbase_red"},
				{"jorj_whittaker_rotor_master", 1, "whittaker_rotor_master"},
				{"jorj_whittaker_rotor_master", 2, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_rotor_master", 3, "whittaker_pillarbase_green"},
				{"jorj_whittaker_rotor_master", 4, "whittaker_pillarbase_white"},
				{"jorj_whittaker_rotor_master", 5, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_pillar_flux", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar_flux", 1, "whittaker_pillar_flux"},
				{"jorj_whittaker_pillar_flux", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar_flux", 3, "whittaker_pillar_flux_red"},
				{"jorj_whittaker_pillar_flux", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar_flux", 5, "whittaker_pillar_flux_pink"},
				{"jorj_whittaker_pillar_flux", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar_flux", 7, "whittaker_pillar_flux_green"},
				{"jorj_whittaker_pillar_flux", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar_flux", 9, "whittaker_pillar_flux_white"},
				{"jorj_whittaker_pillar_flux", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar_flux", 11, "whittaker_pillar_flux_blue"},
				{"jorj_whittaker_pillar_flux", 12, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_screens_flux", 0, "whittaker_screen1"},
				{"jorj_whittaker_screens_flux", 1, "whittaker_screen2"},
				{"jorj_whittaker_screens_flux", 2, "whittaker_screen1_red"},
				{"jorj_whittaker_screens_flux", 3, "whittaker_screen2_red"},
				{"jorj_whittaker_screens_flux", 4, "whittaker_screen1_pink"},
				{"jorj_whittaker_screens_flux", 5, "whittaker_screen2_pink"},
				{"jorj_whittaker_screens_flux", 6, "whittaker_screen1_green"},
				{"jorj_whittaker_screens_flux", 7, "whittaker_screen2_green"},
				{"jorj_whittaker_screens_flux", 8, "whittaker_screen1_white"},
				{"jorj_whittaker_screens_flux", 9, "whittaker_screen2_white"},
				{"jorj_whittaker_screens_flux", 10, "whittaker_screen1_blue"},
				{"jorj_whittaker_screens_flux", 11, "whittaker_screen2_blue"},

				{"jorj_whittaker_pillar3_flux", 0, "whittaker_pillarbase"},
				{"jorj_whittaker_pillar3_flux", 1, "whittaker_pillar_flux"},
				{"jorj_whittaker_pillar3_flux", 2, "whittaker_pillarjoint"},
				{"jorj_whittaker_pillar3_flux", 3, "whittaker_pillar_flux_red"},
				{"jorj_whittaker_pillar3_flux", 4, "whittaker_pillarbase_red"},
				{"jorj_whittaker_pillar3_flux", 5, "whittaker_pillar_flux_pink"},
				{"jorj_whittaker_pillar3_flux", 6, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_pillar3_flux", 7, "whittaker_pillar_flux_green"},
				{"jorj_whittaker_pillar3_flux", 8, "whittaker_pillarbase_green"},
				{"jorj_whittaker_pillar3_flux", 9, "whittaker_pillar_flux_white"},
				{"jorj_whittaker_pillar3_flux", 10, "whittaker_pillarbase_white"},
				{"jorj_whittaker_pillar3_flux", 11, "whittaker_pillar_flux_blue"},
				{"jorj_whittaker_pillar3_flux", 12, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_bluehexextra_flux", 0, "whittaker_bluehexextra_flux"},
				{"jorj_whittaker_bluehexextra_flux", 1, "whittaker_pillarbase"},
				{"jorj_whittaker_bluehexextra_flux", 2, "whittaker_pillarbase_red"},
				{"jorj_whittaker_bluehexextra_flux", 3, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_bluehexextra_flux", 4, "whittaker_pillarbase_green"},
				{"jorj_whittaker_bluehexextra_flux", 5, "whittaker_pillarbase_white"},
				{"jorj_whittaker_bluehexextra_flux", 6, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_bluehexextra_flux_2", 0, "whittaker_bluehexextra_flux"},
				{"jorj_whittaker_bluehexextra_flux_2", 1, "whittaker_pillarbase"},
				{"jorj_whittaker_bluehexextra_flux_2", 2, "whittaker_pillarbase_red"},
				{"jorj_whittaker_bluehexextra_flux_2", 3, "whittaker_pillarbase_pink"},
				{"jorj_whittaker_bluehexextra_flux_2", 4, "whittaker_pillarbase_green"},
				{"jorj_whittaker_bluehexextra_flux_2", 5, "whittaker_pillarbase_white"},
				{"jorj_whittaker_bluehexextra_flux_2", 6, "whittaker_pillarbase_blue"},

				{"jorj_whittaker_fluxpart2_flux", 0, "whittaker_rotor"},
				{"jorj_whittaker_fluxpart2_flux", 1, "whittaker_platform2"},
				{"jorj_whittaker_fluxpart2_flux", 2, "whittaker_rotor_red"},
				{"jorj_whittaker_fluxpart2_flux", 3, "whittaker_rotor_pink"},
				{"jorj_whittaker_fluxpart2_flux", 4, "whittaker_rotor_green"},
				{"jorj_whittaker_fluxpart2_flux", 5, "whittaker_rotor_white"},
				{"jorj_whittaker_fluxpart2_flux", 6, "whittaker_rotor_blue"},

				{"self", 0, "whittaker_floor"},
				{"jorj_whittaker_wall1", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall2", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall3", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall4", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall5", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall6", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall7", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall8", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall5_flux", 0, "whittaker_hexagon"},
				{"jorj_whittaker_wall5_flux", 1, "whittaker_black"},
			},
		},
	},
})