local T = {
	Base = "jorj_whittaker",
	Name = "Series 13",
	ID = "jorj_whittaker_s13",
	IsVersionOf="jorj_whittaker",
	--IsVersionOf = "jorj_whittaker",

}
T.Interior={
	Parts={
		jorj_whittaker_pillarextra_s13={	ang=Angle(0,60,0),pos=Vector(9,9,0)},
		jorj_whittaker_pillarextra_s13_1={	ang=Angle(0,175,0),pos=Vector(21,15,0)},
		jorj_whittaker_pillarextra_s13_2={	ang=Angle(0,297,0),pos=Vector(4,-7,0)},
		jorj_whittaker_switch11_s13={	ang=Angle(0,60,0),pos=Vector(-145,-65,49.2)},
		jorj_whittaker_switch11_s13_1={	ang=Angle(0,173,0),pos=Vector(153,-93,49.8)},
		jorj_whittaker_switch11_s13_2={	ang=Angle(0,300,0),pos=Vector(26,163,49.5)},
		jorj_whittaker_switch13_s13={	ang=Angle(0,65,0),pos=Vector(-148,-67,73)},
        jorj_whittaker_switch13_s13_1={	ang=Angle(0,50,0),pos=Vector(-142,-74,73)},
		jorj_whittaker_switch13_s13_2={	ang=Angle(0,173,0),pos=Vector(161,-86,70.8)},
		jorj_whittaker_switch13_s13_3={	ang=Angle(-20,173,60),pos=Vector(156,-97,74.8)},
		jorj_whittaker_switch13_s13_4={	ang=Angle(0,300,0),pos=Vector(29,165,75)},
		jorj_whittaker_switch13_s13_5={	ang=Angle(0,300,0),pos=Vector(17,165,71)},
		jorj_whittaker_switch8_2 = false,
		jorj_whittaker_sphere_s13={	ang=Angle(0,0,0),pos=Vector(0,0,1)},
		jorj_whittaker_switch14_s13={	ang=Angle(0,0,0),pos=Vector(5,-40,54)},
		jorj_whittaker_sphere_s13_1={	ang=Angle(0,239,0),pos=Vector(6,6,0)},
		jorj_whittaker_switch4=false,
		jorj_whittaker_switch4_2=false,
		jorj_whittaker_switch4_1=false,
		jorj_whittaker_switch15_s13={	ang=Angle(0,0,0),pos=Vector(-4,5,-2)},
		jorj_whittaker_switch15_s13_1={	ang=Angle(0,0,0),pos=Vector(-15,-4,-2)},
		jorj_whittaker_switch15_s13_2={	ang=Angle(0,0,0),pos=Vector(0,0,0)},
		jorj_whittaker_fliplamp_s13={	ang=Angle(0,0,0),pos=Vector(-32,3.5,72)},
		jorj_whittaker_fliplamp_s13_1={	ang=Angle(0,245,0),pos=Vector(28,26.5,72)},
		jorj_whittaker_fliplamp_s13_2={	ang=Angle(0,120,0),pos=Vector(15,-27,70)},
		jorj_whittaker_fliplamp=false,
		jorj_whittaker_fliplamp_2=false,
	},
	TextureSets = {
		["flight"] = {
			prefix = "models/jorj/whittaker/flight_s13/",
		},
	},
}
TARDIS:AddInterior(T)