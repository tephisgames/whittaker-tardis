local PART={}
PART.ID = "jorj_whittaker_switch10"
PART.Name = "switch7"
PART.Model = "models/jorj/whittaker/switch10.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/switch10.wav"

TARDIS:AddPart(PART)