local PART={}
PART.ID = "jorj_whittaker_switch14_s13"
PART.Name = "switch14_s13"
PART.Model = "models/jorj/whittaker/switch14_s13.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/switch14.wav"

TARDIS:AddPart(PART)