local PART={}
PART.ID = "jorj_whittaker_wall5_flux_collider2"
PART.Name = "wall5_flux_collider2"
PART.Model = "models/props_c17/oildrum001.mdl"
PART.AutoSetup = true
PART.Collision = true
function PART:Initialize()
    if CLIENT then return end
    self:SetVisible(false)
end

TARDIS:AddPart(PART)