local PART={}
PART.ID = "jorj_whittaker_switch9"
PART.Name = "switch9"
PART.Model = "models/jorj/whittaker/switch9.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.2
PART.Sound = "jorj/whittaker/switch9.wav"

TARDIS:AddPart(PART)