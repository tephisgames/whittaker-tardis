local PART={}
PART.ID = "jorj_whittaker_cc_dispenser"
PART.Name = "cc_dispenser"
PART.Model = "models/jorj/whittaker/cc_dispenser.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/pedal2.wav"

TARDIS:AddPart(PART)