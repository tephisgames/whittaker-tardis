local PART={}
PART.ID = "jorj_whittaker_switch5_3"
PART.Name = "switch5_3"
PART.Model = "models/jorj/whittaker/switch5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.SoundOff = "jorj/whittaker/switch5_3_off.wav"
PART.SoundOn = "jorj/whittaker/switch5_3_on.wav"

TARDIS:AddPart(PART)