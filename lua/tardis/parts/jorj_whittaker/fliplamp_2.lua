local PART={}
PART.ID = "jorj_whittaker_fliplamp_2"
PART.Name = "fliplamp_2"
PART.Model = "models/jorj/whittaker/fliplamp.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.4
PART.Sound = "jorj/whittaker/fliplamp.wav"

TARDIS:AddPart(PART)