local PART={}
PART.ID = "jorj_whittaker_wall5_flux_collider"
PART.Name = "wall5_flux_collider"
PART.Model = "models/hunter/triangles/trapezium3x3x1a.mdl"
PART.AutoSetup = true
PART.Collision = true
function PART:Initialize()
    if CLIENT then return end
    self:SetVisible(false)
end

TARDIS:AddPart(PART)