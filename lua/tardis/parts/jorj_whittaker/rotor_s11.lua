local PART={}
PART.ID = "jorj_whittaker_rotor_s11"
PART.Name = "rotor_s11"
PART.Model = "models/jorj/whittaker/rotor_s11.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.Use = false
if CLIENT then
	function PART:Initialize()
		self.rotor={}
		self.rotor.pos=0
		self.rotor.mode=1
	end

	function PART:Think()
		local exterior = self.exterior
		local power = exterior:GetData("power-state")

		if power then
			local flight = exterior:GetData("flight")
			local teleport = exterior:GetData("teleport")
			local vortex = exterior:GetData("vortex")
			local active = flight or teleport or vortex

			if self.rotor.pos > 0 or active then
				if self.rotor.pos==0 then
					self.rotor.pos=1
				elseif self.rotor.pos==1 and active then
					self.rotor.pos=0
				end
				self.rotor.pos=math.Approach( self.rotor.pos, self.rotor.mode, FrameTime()*0.4 )
				self:SetPoseParameter( "switch", self.rotor.pos )
			end
		end
	end
end

TARDIS:AddPart(PART)
