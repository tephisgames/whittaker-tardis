local PART={}
PART.ID = "jorj_whittaker_switch4_1"
PART.Name = "switch4_1"
PART.Model = "models/jorj/whittaker/switch4.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "jorj/whittaker/switch2.wav"

TARDIS:AddPart(PART)