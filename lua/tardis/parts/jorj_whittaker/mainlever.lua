local PART={}
PART.ID = "jorj_whittaker_mainlever"
PART.Name = "mainlever"
PART.Model = "models/jorj/whittaker/mainlever.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.2
PART.Sound = "jorj/whittaker/mainlever.wav"

TARDIS:AddPart(PART)