local PART={}
PART.ID = "jorj_whittaker_switch5"
PART.Name = "switch5"
PART.Model = "models/jorj/whittaker/switch5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "jorj/whittaker/switch1.wav"

TARDIS:AddPart(PART)