local PART={}
PART.ID = "jorj_whittaker_switch15_s13_2"
PART.Name = "switch15_s13_2"
PART.Model = "models/jorj/whittaker/switch15_s13.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/switch15.wav"

TARDIS:AddPart(PART)