local PART={}
PART.ID = "jorj_whittaker_switch7"
PART.Name = "switch7"
PART.Model = "models/jorj/whittaker/switch7.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/switch7.wav"

TARDIS:AddPart(PART)