local PART={}
PART.ID = "jorj_whittaker_switch11_s13_2"
PART.Name = "switch11_s13_2"
PART.Model = "models/jorj/whittaker/switch11_s13.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/switch11.wav"

if SERVER then
	function PART:Use(ply)
		TARDIS:Control(self.Control, ply, self)
		local on = not self:GetOn()
		local texture = on and "whittaker_consoleblue_blue" or "whittaker_consoleblue"
		self.interior:ChangeTexture("jorj_whittaker_pillarextra_s13_2", 2, texture, "models/jorj/whittaker/")
	end
end

TARDIS:AddPart(PART)