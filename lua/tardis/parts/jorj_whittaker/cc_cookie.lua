local PART={}
PART.ID = "jorj_whittaker_cc_cookie"
PART.Name = "cc_cookie"
PART.Model = "models/jorj/whittaker/cc_cookie.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/pedal2.wav"


TARDIS:AddPart(PART)

local PART={}
PART.ID = "jorj_whittaker_cc_cookie_hitbox"
PART.Name = "cc_cookie_hitbox"
PART.Model = "models/hunter/plates/plate.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Sound = "jorj/whittaker/pedal2.wav"

if SERVER then
	function PART:Initialize()
		self:SetVisible(false)
		self:SetCollide(self:GetData("jorj_whittaker_cc", false), true)
	end
end

TARDIS:AddPart(PART)