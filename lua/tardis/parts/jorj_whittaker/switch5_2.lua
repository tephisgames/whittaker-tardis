local PART={}
PART.ID = "jorj_whittaker_switch5_2"
PART.Name = "switch5_2"
PART.Model = "models/jorj/whittaker/switch5.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "jorj/whittaker/switch1.wav"

TARDIS:AddPart(PART)