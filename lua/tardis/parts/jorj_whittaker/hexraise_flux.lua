local PART={}
PART.ID = "jorj_whittaker_hexraise_flux"
PART.Name = "hexraise_flux"
PART.Model = "models/jorj/whittaker/hexraise.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = true
PART.AnimateSpeed = 0.4
PART.Use=false
TARDIS:AddPart(PART)