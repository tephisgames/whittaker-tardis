local PART={}
PART.ID = "jorj_whittaker_pedal"
PART.Name = "pedal"
PART.Model = "models/jorj/whittaker/pedal.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/pedal.wav"

TARDIS:AddPart(PART)