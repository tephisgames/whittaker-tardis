local PART={}
PART.ID = "jorj_whittaker_switch3_18"
PART.Name = "switch3_18"
PART.Model = "models/jorj/whittaker/switch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "jorj/whittaker/switch3.wav"

TARDIS:AddPart(PART)