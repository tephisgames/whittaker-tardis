local PART={}
PART.ID = "jorj_whittaker_fliplamp_s13_2"
PART.Name = "fliplamp_s13_2"
PART.Model = "models/jorj/whittaker/fliplamp_s13.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/fliplamp.wav"

TARDIS:AddPart(PART)