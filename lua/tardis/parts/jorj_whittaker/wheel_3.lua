local PART={}
PART.ID = "jorj_whittaker_wheel_3"
PART.Name = "wheel_3"
PART.Model = "models/jorj/whittaker/wheel.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.8
PART.Sound = "jorj/whittaker/wheel.wav"

TARDIS:AddPart(PART)