local PART={}
PART.ID = "jorj_whittaker_hourglass_2"
PART.Name = "hourglass_2"
PART.Model = "models/jorj/whittaker/hourglass.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1
PART.Sound = "jorj/whittaker/hourglass.wav"

TARDIS:AddPart(PART)