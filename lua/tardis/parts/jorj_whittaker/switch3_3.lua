local PART={}
PART.ID = "jorj_whittaker_switch3_3"
PART.Name = "switch3_3"
PART.Model = "models/jorj/whittaker/switch3.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.SoundOff = "jorj/whittaker/switch3_3_off.wav"
PART.SoundOn = "jorj/whittaker/switch3_3_on.wav"

TARDIS:AddPart(PART)