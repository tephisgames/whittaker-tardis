local PART={}
PART.ID = "jorj_whittaker_hexraise"
PART.Name = "hexraise"
PART.Model = "models/jorj/whittaker/hexraise.mdl"
PART.AutoSetup = true
PART.Collision = false
PART.Animate = true
PART.AnimateSpeed = 0.4
PART.Sound = "jorj/whittaker/hexraise.wav"

if SERVER then
	PART.Use = false

	function PART:Toggle()
		self:SetOn(not self:GetOn())
		self:SetCollide(self:GetOn())
		sound.Play(self.Sound, self:GetPos())
	end
end
TARDIS:AddPart(PART)