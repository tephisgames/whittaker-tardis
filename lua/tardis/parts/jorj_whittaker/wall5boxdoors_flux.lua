
local PART={}
PART.ID = "jorj_whittaker_flux_intdoors_2"
PART.Name = "flux_intdoors_2"
PART.Model = "models/jorj/whittaker/int_doors.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.UseTransparencyFix = true
PART.Animate = true
PART.AnimateSpeed = 1.8
PART.SoundOff = "jorj/whittaker/doorclose.wav"
PART.SoundOn = "jorj/whittaker/dooropen.wav"

if SERVER then
	function PART:Use(ply)
		local int = self.interior
		local p = int:GetData("flux_door_position", 0)

		if not self:GetData("doorstatereal")
			and not self:GetOn()
			and int.ApplyFluxPortalState
			and p < 3
		then
			if p == 0 or p == 2 then
				int:ApplyFluxPortalState(1, true)
			elseif p == 1 then
				int:ApplyFluxPortalState(0)
			end

			int:Timer("flux_door_open", 0.1, function()
				self.exterior:ToggleDoor()
			end)

			return false
		end

		self:SetCollide(self:GetOn())

		local other = self.interior:GetPart("jorj_whittaker_flux_intdoors_1")
		if self:GetData("jwhittaker_sync_int_doors", false) and IsValid(other) then
			other:Toggle(not self:GetOn())
		end

		if self:GetOn() and IsValid(other) and not other:GetOn() and not self:GetData("doorstatereal") then
			local close_time = int.metadata.Exterior.DoorAnimationTime
			int:Timer("jwhittaker_doors_close", close_time, int.change_state)
		end
	end

	function PART:Toggle( bEnable, ply )
		self:SetOn(bEnable)
		self:SetCollide(not bEnable)
		sound.Play(bEnable and self.SoundOn or self.SoundOff, self:GetPos())
	end
end
TARDIS:AddPart(PART,e)