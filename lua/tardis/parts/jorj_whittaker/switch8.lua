local PART={}
PART.ID = "jorj_whittaker_switch8"
PART.Name = "switch8"
PART.Model = "models/jorj/whittaker/switch8.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 4
PART.Sound = "jorj/whittaker/switch1.wav"

TARDIS:AddPart(PART)