local PART={}
PART.ID = "jorj_whittaker_switch13_s13_6"
PART.Name = "switch13_s13_6"
PART.Model = "models/jorj/whittaker/switch13_s13.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 1.5
PART.Sound = "jorj/whittaker/switch2.wav"

TARDIS:AddPart(PART)