local PART={}
PART.ID = "jorj_whittaker_switch1_1"
PART.Name = "switch1_1"
PART.Model = "models/jorj/whittaker/switch1.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.2
PART.Sound = "jorj/whittaker/switch1.wav"

TARDIS:AddPart(PART)