local PART={}
PART.ID = "jorj_whittaker_wheel_1"
PART.Name = "wheel_1"
PART.Model = "models/jorj/whittaker/wheel.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 0.8
PART.Sound = "jorj/whittaker/wheel.wav"

TARDIS:AddPart(PART)