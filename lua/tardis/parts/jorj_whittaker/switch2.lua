local PART={}
PART.ID = "jorj_whittaker_switch2"
PART.Name = "switch2"
PART.Model = "models/jorj/whittaker/switch2.mdl"
PART.AutoSetup = true
PART.Collision = true
PART.Animate = true
PART.AnimateSpeed = 2.2
PART.Sound = "jorj/whittaker/switch2.wav"

TARDIS:AddPart(PART)